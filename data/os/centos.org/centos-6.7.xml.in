<libosinfo version="0.0.1">
<!-- Licensed under the GNU General Public License version 2 or later.
     See http://www.gnu.org/licenses/ for a copy of the license text -->
  <os id="http://centos.org/centos/6.7">
    <short-id>centos6.7</short-id>
    <_name>CentOS 6.7</_name>
    <version>6.7</version>
    <_vendor>CentOS</_vendor>
    <family>linux</family>
    <distro>centos</distro>
    <upgrades id="http://centos.org/centos/6.6"/>
    <clones id="http://redhat.com/rhel/6.7"/>

    <release-date>2015-08-07</release-date>
    <eol-date>2016-05-25</eol-date>

    <!-- DVD -->
    <media arch="i686">
      <iso>
        <system-id>LINUX</system-id>
        <volume-id>CentOS_6.7_Final</volume-id>
        <volume-size>3786407936</volume-size>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>
    <media arch="x86_64">
      <iso>
        <system-id>LINUX</system-id>
        <volume-id>CentOS_6.7_Final</volume-id>
        <volume-size>3895459840</volume-size>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>

    <!-- Live CD -->
    <media arch="i686" live="true">
      <iso>
        <system-id>LINUX</system-id>
        <volume-id>CentOS-6.7-i386-LiveCD</volume-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>
    <media arch="x86_64" live="true">
      <iso>
        <system-id>LINUX</system-id>
        <volume-id>CentOS-6.7-x86_64-LiveCD</volume-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>

    <!-- Live DVD -->
    <media arch="i686" live="true">
      <iso>
        <system-id>LINUX</system-id>
        <volume-id>CentOS-6.7-i386-LiveDVD</volume-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>
    <media arch="x86_64" live="true">
      <iso>
        <system-id>LINUX</system-id>
        <volume-id>CentOS-6.7-x86_64-LiveDVD</volume-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>

    <!-- Minimal Installer -->
    <media arch="i686">
      <iso>
        <system-id>LINUX</system-id>
        <volume-id>CentOS_6.7_Final</volume-id>
        <volume-size>367001600</volume-size>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>
    <media arch="x86_64">
      <iso>
        <system-id>LINUX</system-id>
        <volume-id>CentOS_6.7_Final</volume-id>
        <volume-size>414187520</volume-size>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>

    <!-- Network Installer -->
    <media arch="i686">
      <iso>
        <system-id>LINUX</system-id>
        <volume-id>CentOS</volume-id>
        <volume-size>196083712</volume-size>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>
    <media arch="x86_64">
      <iso>
        <system-id>LINUX</system-id>
        <volume-id>CentOS</volume-id>
        <volume-size>241172480</volume-size>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>

    <tree arch="x86_64">
      <url>http://vault.centos.org/6.7/os/x86_64/</url>
      <treeinfo>
        <family>CentOS</family>
        <version>6.7</version>
        <arch>x86_64</arch>
      </treeinfo>
    </tree>
    <tree arch="i686">
      <url>http://vault.centos.org/6.7/os/i386/</url>
      <treeinfo>
        <family>CentOS</family>
        <version>6.7</version>
        <arch>i386</arch>
      </treeinfo>
    </tree>

    <resources arch="all">
      <minimum>
        <n-cpus>1</n-cpus>
        <ram>536870912</ram>
      </minimum>

      <recommended>
        <cpu>400000000</cpu>
        <ram>1073741824</ram>
        <storage>9663676416</storage>
      </recommended>
    </resources>

    <installer>
      <script id='http://centos.org/centos/kickstart/jeos'/>
      <script id='http://centos.org/centos/kickstart/desktop'/>
    </installer>
  </os>
</libosinfo>
